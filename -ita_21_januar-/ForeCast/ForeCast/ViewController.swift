//
//  ViewController.swift
//  ForeCast
//
//  Created by Apple on 12/27/21.
//  Copyright © 2021 alvm. All rights reserved.
//

import UIKit

// U assets nalaze se slike koje su imenovane prema nazivima slika koje se dobijaju u okviru json odgovora, to je vrednost iz polja icon

class ViewController: UIViewController {

    // Text field za unos naziva grada
    // Taster za pretragu grada
    @IBOutlet weak var tfCityName: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    // Label za prikaz temperature
    @IBOutlet weak var lblTemperature: UILabel!
    // image view za prikaz slike
    @IBOutlet weak var imageViewIcon: UIImageView!
    // Label za prikaz opisa
    @IBOutlet weak var lblSummary: UILabel!
    
    // Label za prikaz naziva grada
    @IBOutlet weak var lblCityName: UILabel!
    // Label za prikaz naziva drzava
    @IBOutlet weak var lblCountry: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    // Taster za pretragu grada
    @IBAction func btnSubmitTapped(_ sender: Any) {
        
    }
    

}

