//
//  Animacija2ViewController.swift
//  animacije
//
//  Created by Aleksandra Lazarevic on 12.4.22..
//

import UIKit

class Animacija2ViewController: UIViewController {

    @IBOutlet weak var imgviewCar: UIImageView! // outlet za automobil
    
    @IBOutlet weak var vwCilj: UILabel! // outlet za cilj
    
    @IBOutlet weak var imgviewHelikopter: UIImageView! // outlet za helikopter
    
    @IBOutlet weak var imgviewHeliodrom: UIImageView! // outlet za heliodrom
    
    @IBOutlet weak var imgViewMario: UIImageView! // outlet za maria
    
    @IBOutlet weak var imgViewPecurka: UIImageView! // outlet za pecurku
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // akciona metoda za pomeranje automobila
    @IBAction func btnDriveCar(_ sender: Any) {
 
    }

    // akciona metoda za pomeranje helikoptera
    @IBAction func btnHelicopterTapped(_ sender: Any) {

    }
    
    // akciona metoda za pomeranje maria
    @IBAction func btnRunTapped(_ sender: Any) {

    }
}

