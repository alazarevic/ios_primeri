//
//  ViewController.swift
//  animacije
//
//  Created by Aleksandra Lazarevic on 12.4.22..
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var vwBlue: UIView!
    
    @IBOutlet weak var vwPurple: UIView!
    
    @IBOutlet weak var vwOrange: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnPromenaBojeTapped(_ sender: Any) {

    }
    
    @IBAction func btnScaleTapped(_ sender: Any) {

    }
    
    @IBAction func btnRotateTapped(_ sender: Any) {
        
    }
    
    @IBAction func btnTranslateTapped(_ sender: Any) {
        
    }
}

