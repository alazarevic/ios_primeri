//
//  ViewController.swift
//  animacije
//
//  Created by Aleksandra Lazarevic on 12.4.22..
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var vwBlue: UIView!
    
    @IBOutlet weak var vwPurple: UIView!
    
    @IBOutlet weak var vwOrange: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnPromenaBojeTapped(_ sender: Any) {
        UIView.animate(withDuration: 3) {
            self.vwBlue.backgroundColor = .red
        } completion: { _ in
            UIView.animate(withDuration: 3) {
                self.vwBlue.backgroundColor = .systemBlue
            }
        }
    }
    
    @IBAction func btnScaleTapped(_ sender: Any) {
        
        let scale = CGAffineTransform(scaleX: 2, y: 2)
        
        UIView.animate(withDuration: 3) {
            self.vwPurple.transform = scale
        }
    }
    
    @IBAction func btnRotateTapped(_ sender: Any) {
        
        let rotate = CGAffineTransform(rotationAngle: .pi)
        
        UIView.animate(withDuration: 3) {
            self.vwBlue.transform = rotate
        }
        
    }
    
    @IBAction func btnTranslateTapped(_ sender: Any) {
        
        let translate = CGAffineTransform(translationX: 100, y: 50)
        
        UIView.animate(withDuration: 3) {
            self.vwOrange.transform = translate
        }
        
    }
}

