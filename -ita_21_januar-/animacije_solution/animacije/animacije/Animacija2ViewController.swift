//
//  Animacija2ViewController.swift
//  animacije
//
//  Created by Aleksandra Lazarevic on 12.4.22..
//

import UIKit

class Animacija2ViewController: UIViewController {

    @IBOutlet weak var imgviewCar: UIImageView!
    
    @IBOutlet weak var vwCilj: UILabel!
    
    @IBOutlet weak var imgviewHelikopter: UIImageView!
    
    @IBOutlet weak var imgviewHeliodrom: UIImageView!
    
    @IBOutlet weak var imgViewMario: UIImageView!
    
    @IBOutlet weak var imgViewPecurka: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func btnDriveCar(_ sender: Any) {
        let translate = CGAffineTransform(translationX: vwCilj.frame.minX, y: 0)
        
        let pocetnoStanje = imgviewCar.frame.minX
        
        UIView.animate(withDuration: 2) {
            self.imgviewCar.transform = translate
        } completion: { _ in
            // rikverc
            UIView.animate(withDuration: 2) {
                self.imgviewCar.transform = CGAffineTransform(translationX: -pocetnoStanje, y: 0)
            }
        }
    }

    @IBAction func btnHelicopterTapped(_ sender: Any) {
        
        let translate = CGAffineTransform(translationX: -abs(self.imgviewHelikopter.frame.origin.x - self.imgviewHeliodrom.frame.origin.x), y: self.imgviewHeliodrom.frame.origin.y - self.imgviewHelikopter.frame.origin.y)
        
        UIView.animate(withDuration: 2) {
             self.imgviewHelikopter.transform = translate
        }
    }
    
    
    @IBAction func btnRunTapped(_ sender: Any) {
        let translate = CGAffineTransform(translationX: 130, y: 0)
        let scale = CGAffineTransform.init(scaleX: 2, y: 2)
        
        UIView.animate(withDuration: 2) {
            self.imgViewMario.transform = translate.concatenating(scale)
        }
    }
}

