//
//  Weather.swift
//  forecastExample
//
//  Created by User on 14/6/21.
//  Copyright © 2021 Example. All rights reserved.
//

import Foundation

//struct Weather {
//    var timeZone: String
//    var summary: String
//    var iconName: String
//    var temperature: Double
//
//    
//    enum SerializationError: Error {
//        case missing(String)
//        case invalid(String,Any)
//    }
//    init(json:[String:Any]) throws{
//        guard let timeZone = json["timezone"] as? String else {throw SerializationError.missing("No timezone")}
//        guard let summary = json["summary"] as? String else {throw SerializationError.missing("No summary")}
//        guard let iconName = json["icon"] as? String else {throw SerializationError.missing("No icon")}
//        guard let temperature = json["temperature"] as? Double else {throw SerializationError.missing("No temperature")}
//        
//        self.timeZone = timeZone
//        self.summary = summary
//        self.iconName = iconName
//        self.temperature = temperature
//    }
//
//}
//
//    func createURL() -> URL? {
//        
//        let basicURL = "https://api.darksky.net/forecast/"
//        
//        let apiKey = "2ea6e0339742ff9674097cd61ff91778"
//
//        let latitude = "51.509865"
//        let longitude = "-0.118092"
//        return URL(string: basicURL + apiKey + "/" + latitude + "," + longitude)
//        
//    }
//    func requestInfo(completion: @escaping([Weather]?)->Void) {
//        
//        let url = createURL()
//                          
//        let task = URLSession.shared.dataTask(with: url!) { (data:Data?, response:URLResponse?, error: Error?) in
//         print(url!)
//        
//            var weatherArray:[Weather] = []
//            if let data = data {
//                do {
//                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] {
//                        if let currently = json["currently"] as? [String:Any] {
//                            if let weatherObject = try? Weather(json: currently) {
//                                weatherArray.append(weatherObject)
//                            }
//                        }
//                    }
//                }
//                catch{
//                    print(error.localizedDescription)
//                }
//            }
//     }
//     task.resume()
//}
