//
//  ViewController.swift
//  forecastExample
//
//  Created by User on 14/6/21.
//  Copyright © 2021 Example. All rights reserved.
//

import UIKit

struct City {
    var name: String
    var latitude: Double
    var longitude: Double
}

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var lblTimeZone: UILabel!
    
    @IBOutlet weak var lblSummary: UILabel!
    
    @IBOutlet weak var imgViewIcon: UIImageView!
    
    @IBOutlet weak var lblTemperature: UILabel!
    
    @IBOutlet weak var cityPicker: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.barStyle = .default
        
        let textColor: UIColor = .white
        cityPicker.setValue(textColor, forKey: "textColor")
        let middleRow = (cities.count / 2) - 1
        cityPicker.selectRow(middleRow, inComponent: 0, animated: true)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        updateWeather()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cities.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let city = cities[row]
        return city.name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        updateWeather()
    }

    var cities = [
        City(name: "London", latitude: 51.509865, longitude: -0.118092),
        City(name: "Moscow", latitude: 55.752222, longitude: 37.615556),
        City(name: "New York", latitude: 40.6943, longitude: -73.9249),
        City(name: "Tokyo", latitude: 35.6850, longitude: 139.7514),
        City(name: "Bucharest", latitude: 44.433333, longitude: 26.1),
        City(name: "Belgrade", latitude: 44.787197, longitude: 20.457273),
        City(name: "Berlin", latitude: 52.516667, longitude: 13.4),
        City(name: "Madrid", latitude: 40.412752, longitude: -3.707721),
        City(name: "Rome", latitude:  41.902782, longitude: 12.496366),
        City(name: "Paris", latitude: 48.866667, longitude: 2.333333),
        City(name: "Sarajevo", latitude: 43.85, longitude: 18.383333),
        City(name: "Chisinau", latitude: 47.005556, longitude: 28.8575)
    ]
    
    func createURL() -> URL {
        
        let basicURL = "https://api.darksky.net/forecast/"
        let apiKey = "2ea6e0339742ff9674097cd61ff91778"
        
        var latitude: String = ""
        var longitude: String = ""
        
        let selectedRow = cityPicker.selectedRow(inComponent: 0)
        let title = pickerView(cityPicker, titleForRow: selectedRow, forComponent: 0)

        for city in cities {
            if city.name == title {
                latitude = String(city.latitude)
                longitude = String(city.longitude)
            }
        }
        
        
        print(URL(string: basicURL + apiKey + "/" + latitude + "," + longitude)!)
        
        return URL(string: basicURL + apiKey + "/" + latitude + "," + longitude)!
    }
    
    func updateWeather(){
     // connect to API and load weather for given city
        
        lblTimeZone.text = "..."
        lblTemperature.text = "..."
        lblSummary.text = "loading weather..."
    
        
        let url = createURL()
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data {
                do {
                    if let jsonPodaci = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] {
                        DispatchQueue.main.async {
                            self.lblTimeZone.text = jsonPodaci["timezone"] as! String
                            if let currently = jsonPodaci["currently"] as? [String:AnyObject] {
                                self.lblSummary.text =  currently["summary"] as! String
                                let nazivSlike = currently["icon"] as! String
                                self.imgViewIcon.image = UIImage(named: nazivSlike)
                                
                                //String(format: "%.0f", (temp - 32) / 1.8)
                                let tempF = currently["temperature"] as! Double
                                let tempC = (tempF - 32) / 1.8
                                self.lblTemperature.text = String(format: "%.0f", tempC) + " °C"
                            }
                        }

                    }
                }
                catch {
                    print("Error...")
                }
            }
        }
        
//        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
//          // check if data is valid and not nil
//            if let usableData = data {
//                do {
//                    // using JsonSerialization
//                    if let jsonData = try JSONSerialization.jsonObject(with: usableData, options: []) as? [String: AnyObject] {
//        
//                        // update UI with our data
//                        // dispatch is required
//                        DispatchQueue.main.async {
//                            if let timeZone = jsonData["timezone"] as? String {
//                                self.lblTimeZone.text = timeZone
//                            }
//                            if let currently = jsonData["currently"] as? [String: AnyObject]{
//                                self.lblSummary.text = currently["summary"] as? String
//                                let temp = (currently["temperature"] as? Double)!
//                                self.lblTemperature.text = String(format: "%.0f", (temp - 32) / 1.8) + " °C"
//                                self.imgViewIcon.image = UIImage(named: (currently["icon"] as? String)!)
//                            }
//                        }
//                    }
//                } catch let myJSONError {
//                    print(myJSONError)
//                }
//            }
//        }
        task.resume()
        
    }
}


