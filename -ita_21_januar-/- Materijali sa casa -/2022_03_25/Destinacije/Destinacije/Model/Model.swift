//
//  Model.swift
//  Destinacije
//
//  Created by Gupa 1 on 18.3.22..
//

import Foundation

struct Destinacija: Codable {
    var naziv: String
    var nazivSlike: String
    var cena: Int
    
    // ako ne postoje destinacije u skladistu - ucitaj probne
    // ako postoje u skladistu - procitaj njih
    // sacuvaj destinacije u skladistu
    
    static func ucitajProbnePodatke () -> [Destinacija] {
        return [
            Destinacija(naziv: "Beograd", nazivSlike: "beograd", cena: 500),
            Destinacija(naziv: "Madrid", nazivSlike: "madrid", cena: 500),
            Destinacija(naziv: "Lisabon", nazivSlike: "lisabon", cena: 600),
            Destinacija(naziv: "Sarajevo", nazivSlike: "sarajevo", cena: 500),
            Destinacija(naziv: "Berlin", nazivSlike: "berlin", cena: 700),
            Destinacija(naziv: "New York", nazivSlike: "newyork", cena: 900)
        ]
    }
    
    static var storageURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("destinacije").appendingPathExtension("plist")
    
    // prihvata niz destinacija za cuvanje u skladistu
    static func sacuvajDestinacije (dest: [Destinacija]) {
        
        let encoder = PropertyListEncoder()
        let encDest = try! encoder.encode(dest) // destinacije u bajtove - 304 bytes
        // upisivanje enkovodvanih destinacija u storage
        try! encDest.write(to: storageURL)
    }
    
    static func procitajDestinacije () -> [Destinacija]? {
        
        let decoder = PropertyListDecoder()
        // trazim podatke na lokaciji u skladistu
        let podaci = try? Data(contentsOf: storageURL)
        
        //ako postoje podaci, prebaci ih iz bajtova u destinacije
        if let podaci = podaci {
            let procitaneDest = try! decoder.decode(Array<Destinacija>.self, from: podaci)
            return procitaneDest
        }
        else {
            return nil
        }
    }
}

