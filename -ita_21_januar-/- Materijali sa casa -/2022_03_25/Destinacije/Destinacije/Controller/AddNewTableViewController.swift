//
//  AddNewTableViewController.swift
//  Destinacije
//
//  Created by Gupa 1 on 23.3.22..
//

import UIKit

class AddNewTableViewController: UITableViewController {
    
    @IBOutlet weak var tfNaziv: UITextField!
    
    @IBOutlet weak var tfCena: UITextField!
    
    @IBOutlet weak var tfNazivSlike: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    var novaDestinacija: Destinacija?

    @IBAction func btnDoneTapped(_ sender: Any) {
        
        if tfNaziv.text! != "" && tfCena.text != "" && tfNazivSlike.text! != "" {
            novaDestinacija = Destinacija(naziv: tfNaziv.text!, nazivSlike: tfNazivSlike.text!, cena: Int(tfCena.text!)!)
            performSegue(withIdentifier: "povratak", sender: self)
        }
    }
    
}
