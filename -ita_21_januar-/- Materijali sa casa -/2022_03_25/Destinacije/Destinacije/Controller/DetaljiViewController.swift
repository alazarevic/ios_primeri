//
//  DetaljiViewController.swift
//  Destinacije
//
//  Created by Gupa 1 on 23.3.22..
//

import UIKit

class DetaljiViewController: UIViewController {

    @IBOutlet weak var lblNaziv: UILabel!
    
    @IBOutlet weak var lblCena: UILabel!
    
    @IBOutlet weak var destImageView: UIImageView!
    
    var odabranaDestinacija: Destinacija?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let odabranaDestinacija = odabranaDestinacija {
            
            lblNaziv.text = odabranaDestinacija.naziv
            lblCena.text = "\(odabranaDestinacija.cena) $"
            destImageView.image = UIImage(named: odabranaDestinacija.nazivSlike)
            
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
