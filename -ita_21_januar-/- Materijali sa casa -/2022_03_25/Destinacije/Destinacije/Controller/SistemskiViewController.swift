//
//  SistemskiViewController.swift
//  Destinacije
//
//  Created by Gupa 1 on 23.3.22..
//

import UIKit
import SafariServices
import MessageUI

class SistemskiViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var slikaImgView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnShareTapped(_ sender: Any) {
        
        if let slika = slikaImgView.image {
            
            let actVC = UIActivityViewController(activityItems: [slika], applicationActivities: nil)
            
            present(actVC, animated: true, completion: nil)
            
        }
        
    }
    
    @IBAction func btnSafariTapped(_ sender: Any) {
        
        let safariVC = SFSafariViewController(url: URL(string: "http://www.it-akademija.com")!)
        
        present(safariVC, animated: true, completion: nil)
    }
    
    
    @IBAction func btnAlertTapped(_ sender: Any) {
        
        let alert = UIAlertController(title: "Ovo je naslov", message: "Ovo je poruka...", preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Ok", style: .default) { _ in
            // sta ce se desiti kad kliknem na akciju
            print("Pritisnuli ste ok.")
            
        }
        
        alert.addAction(action1)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnCameraTapped(_ sender: Any) {
        
        let imgPicker = UIImagePickerController()
        imgPicker.delegate = self
        
        let alert = UIAlertController(title: "Choose image source...", message: nil, preferredStyle: .actionSheet)
        
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            
            let photoAction = UIAlertAction(title: "Photo Library", style: .default) { _ in
                
                imgPicker.sourceType = .photoLibrary
                
                self.present(imgPicker, animated: true, completion: nil)
                
            }
            alert.addAction(photoAction)
            
        }
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            
            let cameraAction = UIAlertAction(title: "Camera", style: .default) { _ in
                
                imgPicker.sourceType = .camera
                
                self.present(imgPicker, animated: true, completion: nil)

            }
            alert.addAction(cameraAction)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        
        
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let odabranaSlika = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            
            slikaImgView.image = odabranaSlika
            
            // spustanje prozora, suprotno od present
            picker.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
    @IBAction func btnEmailTapped(_ sender: Any) {
        
        if MFMailComposeViewController.canSendMail() == false {
            print("Nije moguce slanje emaila.")
        }
        else {
            print("Mozete slati email")
            
            let emailVC = MFMailComposeViewController()
            
            emailVC.setSubject("Ovo je naslov poruke")
            emailVC.setToRecipients(["aleksandra.lazarevic@link.co.rs"])
            
            present(emailVC, animated: true, completion: nil)
            
        }
        
    }
    
}
