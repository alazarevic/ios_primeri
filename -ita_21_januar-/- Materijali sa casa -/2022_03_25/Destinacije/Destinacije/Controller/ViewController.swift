//
//  ViewController.swift
//  Destinacije
//
//  Created by Gupa 1 on 18.3.22..
//

import UIKit

// Klasa koja opisuje celiju

class DestinacijaCell: UITableViewCell {
    
    @IBOutlet weak var destImageView: UIImageView!
    @IBOutlet weak var lblNaziv: UILabel!
    @IBOutlet weak var lblCena: UILabel!
    
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var destinacije = [Destinacija]()

    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var btnEdit: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.isEditing = false
        
        if Destinacija.procitajDestinacije() != nil {
            destinacije = Destinacija.procitajDestinacije()!
        }
        else {
            destinacije = Destinacija.ucitajProbnePodatke()
        }
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return destinacije.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let celija = tableView.dequeueReusableCell(withIdentifier: "destinacijaCell") as! DestinacijaCell
        
        let podatakZaCeliju = destinacije[indexPath.row]
                                            // 0, 1, 2, 3, ...
        celija.destImageView.image = UIImage(named: podatakZaCeliju.nazivSlike)
        celija.lblNaziv.text = podatakZaCeliju.naziv
        celija.lblCena.text = "\(podatakZaCeliju.cena) $"
        
        return celija
    }

    var selektovanGrad: Destinacija?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Selektovana celija")
        print("Red: \(indexPath.row)")
        selektovanGrad = destinacije[indexPath.row]
        performSegue(withIdentifier: "detalji", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detalji" {
            let odrediste = segue.destination as! DetaljiViewController
            odrediste.odabranaDestinacija = selektovanGrad!
        }
    }
    
    
    @IBAction func btnEditTapped(_ sender: Any) {
        tableView.isEditing = !tableView.isEditing
        
        if tableView.isEditing == true {
            btnEdit.title = "Done"
        } else {
            btnEdit.title = "Edit"
        }
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        
        let destinacijaPremestanje = destinacije[sourceIndexPath.row]
        destinacije.remove(at: sourceIndexPath.row)
        destinacije.insert(destinacijaPremestanje, at: destinationIndexPath.row)
        
        Destinacija.sacuvajDestinacije(dest: destinacije)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {

        if editingStyle == .delete {
            destinacije.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            Destinacija.sacuvajDestinacije(dest: destinacije)
        }
        
    }
    
    @IBAction func unwindPovratak (_ unwindSegue: UIStoryboardSegue) {
        
        if unwindSegue.identifier == "povratak" {
            
            let unosKontroler = unwindSegue.source as! AddNewTableViewController
            
            let novaLokacija = IndexPath(row: destinacije.count, section: 0)
            let novaDestinacija = unosKontroler.novaDestinacija!
            destinacije.append(novaDestinacija)
            tableView.insertRows(at: [novaLokacija], with: .automatic)
            Destinacija.sacuvajDestinacije(dest: destinacije)
           // tableView.reloadData()
            
        }
        
    }
    
}

