//
//  itp_09_ex06.swift
//  
//
//  Created by Aleksandra Lazarevic on 4/9/21.
//

import Foundation
var playersLives = 5

while playersLives > 0 {
    print("Player is still in the game")
    playersLives -= 1
}
print("Game over")
