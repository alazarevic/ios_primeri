//
//  itp_09_ex05.swift
//  
//
//  Created by Aleksandra Lazarevic on 4/9/21.
//

import Foundation
let h = 10
var a = 0

for _ in 0...h{
    for _ in 0...a{
        print("#", terminator:" ")
    }
    a+=1
    print("")
}
