//
//  itp_08_ex03.swift
//  
//
//  Created by Aleksandra Lazarevic on 3/31/21.
//

import Foundation

func myFunction(a:Int)->Bool{
    if a%2 == 0 {
        return true
    }
    else {
        return false
    }
}
let res = myFunction(a:10)
print(res)
