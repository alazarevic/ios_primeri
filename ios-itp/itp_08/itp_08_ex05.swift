//
//  itp_08_ex05.swift
//  
//
//  Created by Aleksandra Lazarevic on 3/31/21.
//

import Foundation

func calculator(a: Double, b: Double, operation: String)->Double{
    switch operation{
    case "+":
        return a + b
    case "-":
        return a - b
    case "*":
        return a * b
    case "/":
        return a / b
    default:
        print("Invalid operation.")
        return 0.0
    }
}

var res = calculator(a:10.0, b:12.0, operation: "+")
print(res)
