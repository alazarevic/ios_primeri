//
//  itp_07_ex08.swift
//  
//
//  Created by Aleksandra Lazarevic on 3/30/21.
//

import Foundation

var ocena: Int = 5

switch ocena {
case 1:
    print("Nedovoljan")
case 2:
    print("Dovoljan")
case 3:
    print("Dobar")
case 4:
    print("Vrlo dobar")
case 5:
    print("Odlican")
default:
    print("Nepoznata ocena")
}
