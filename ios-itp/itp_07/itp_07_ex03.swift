//
//  itp_06_ex05.swift
//  
//
//  Created by Aleksandra Lazarevic on 3/30/21.
//

import Foundation

var temperature: Int = 20

if temperature > 20 {
    print("T-shirt")
}
else {
    print("Jacket")
}
