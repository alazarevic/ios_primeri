//
//  itp_06_ex05.swift
//  
//
//  Created by Aleksandra Lazarevic on 3/30/21.
//

import Foundation

var isDriving: Bool = true
var speed: Int = 70

if(isDriving == true){
    if(speed > 50) {
        print("You have exceeded the speed limit")
    }
    else {
        print("Speed is ok.")
}
}
else {
    print("The car is on the parking lot.")
}
