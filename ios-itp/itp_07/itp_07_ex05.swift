//
//  itp_06_ex05.swift
//  
//
//  Created by Aleksandra Lazarevic on 3/30/21.
//

import Foundation

var number: Int = 5

if number % 2 == 0{
    print("Number is even.")
}
else {
    print("Number is odd")
}
