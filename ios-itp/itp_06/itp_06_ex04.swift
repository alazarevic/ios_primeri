//
//  itp_06_ex02.swift
//  
//
//  Created by Aleksandra Lazarevic on 3/30/21.
//

import Foundation

//Provera da li je broj paran ili neparan

let broj = 10

let ostatakPriDeljenju = 10 % 2

print(ostatakPriDeljenju == 0)

if (neki uslov) {
    //if blok koda...
} else {
    //else blok koda...
}
