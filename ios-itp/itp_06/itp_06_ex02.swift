//
//  itp_06_ex02.swift
//  
//
//  Created by Aleksandra Lazarevic on 3/30/21.
//

import Foundation

let heartRate1: Int = 60
let heartRate2: Int = 85
let heartRate3: Int = 95

let heartRates = heartRate1 + heartRate2 + heartRate3

let averageHR = heartRates / 3

print("Average heart rate is \(averageHR).")
