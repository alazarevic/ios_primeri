//
//  ViewController.swift
//  game
//
//  Created by User on 8/30/19.
//  Copyright © 2019 Example. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var lblComputer: UILabel!
    @IBOutlet weak var lblYou: UILabel!
    @IBOutlet weak var lblComputerChoice: UILabel!
    @IBOutlet weak var lblYourChoice: UILabel!
    @IBOutlet weak var lblResult: UILabel!
    
    @IBOutlet weak var btnPaper: UIButton!
    @IBOutlet weak var btnRock: UIButton!
    @IBOutlet weak var btnScissors: UIButton!
    
    @IBOutlet weak var lblComputerPoints: UILabel!
    @IBOutlet weak var lblYourPoints: UILabel!
    
    /// Enumeracija koja sadrzi ponudjene opcije u igri
    enum Choices: String {
        case paper = "✋🏻"
        case rock = "👊🏻"
        case scissors = "✌🏻"
    }
    
    /// Enumeracija koja sadrzi moguca stanja igre (pobeda korisnika, pobeda racunara, izjednaceno
    enum GameStatus {
        case yourVictory
        case computersVictory
        case even
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /// kada se view ucita prvi put izvrsava se telo viewDidLoad metode
        setUpButtons()
        setupLabels()
    }
    
    /// metod koji postavlja na svaki taster po jednu od opcija za odabir
    func setUpButtons() {
        btnPaper.setTitle(Choices.paper.rawValue, for: .normal)
        btnRock.setTitle(Choices.rock.rawValue, for: .normal)
        btnScissors.setTitle(Choices.scissors.rawValue, for: .normal)
    }
    
    /// metod koji sredjuje izgled label kontrola
    func setupLabels() {
        
        // omogucava izmenu izgleda labela, npr dodavanje zaobjenih ivica
        lblYourChoice.layer.masksToBounds = true
        lblYourChoice.layer.cornerRadius = 10
        
        lblComputerChoice.layer.masksToBounds = true
        lblComputerChoice.layer.cornerRadius = 10
        
        lblYou.layer.masksToBounds = true
        lblYou.layer.cornerRadius = 10
        
        lblComputer.layer.masksToBounds = true
        lblComputer.layer.cornerRadius = 10
        
        lblResult.layer.masksToBounds = true
        lblResult.layer.cornerRadius = 10
        
        lblYourChoice.text = ""
        lblComputerChoice.text = ""
        lblResult.text = "Let's play! Make choice..."
        
        lblComputerPoints.text = "0"
        lblYourPoints.text = "0"

    }
    
    /// metod kreira odabir racunara. Bira se nasumican clan niza tako sto se dobije random broj od 0 do broj clanova niza -1 i on se iskoristi kao nasumican indeks
    func getComputerChoice() -> Choices {
        let choicesArray: [Choices] = [.paper, .rock, .scissors]
        let choiceIndex = Int.random(in: 0...choicesArray.count - 1)
        let computerChoice: Choices = choicesArray[choiceIndex]
        return computerChoice
    }
    
    /// akciona metoda koja se izvrsava kada se klikne na taster za odabir papira
    @IBAction func btnPaperPressed(_ sender: Any) {
        lblYourChoice.text = Choices.paper.rawValue
        lblComputerChoice.text = getComputerChoice().rawValue
        getResult()
    }
    
    /// akciona metoda koja se izvrsava kada se klikne na taster za odabir kamena
    @IBAction func btnRockPressed(_ sender: Any) {
        lblYourChoice.text = Choices.rock.rawValue
        lblComputerChoice.text = getComputerChoice().rawValue
        getResult()
    }
    
    /// akciona metoda koja se izvrsava kada se klikne na taster za odabir makaza
    @IBAction func btnScissorsPressed(_ sender: Any) {
        lblYourChoice.text = Choices.scissors.rawValue
        lblComputerChoice.text = getComputerChoice().rawValue
        getResult()
    }
    
    /// metod koji proverava odabrane vrednosti i u zavisnosti od toga odredjuje konacan ishod igre
    func getResult() {
        
        var yourPoints: Int = Int(lblYourPoints!.text!)!
        var computerPoints: Int = Int(lblComputerPoints!.text!)!

        var status: GameStatus
        if lblYourChoice.text == Choices.paper.rawValue && lblComputerChoice.text == Choices.rock.rawValue {
            status = GameStatus.yourVictory
        }
        else if lblYourChoice.text == Choices.scissors.rawValue && lblComputerChoice.text == Choices.paper.rawValue {
            status = GameStatus.yourVictory
        }
        else if lblYourChoice.text == Choices.rock.rawValue && lblComputerChoice.text == Choices.scissors.rawValue {
            status = GameStatus.yourVictory
        }
        else if lblYourChoice.text == lblComputerChoice.text {
            status = GameStatus.even
        }
        else {
            status = GameStatus.computersVictory
        }
        
        /// provera statusa igre i postavljanje boje pozadine i uvecavanje poena
        switch status {
        case .yourVictory:
            lblResult.text = "You WIN!"
            view.backgroundColor = .green
            yourPoints += 1
            lblYourPoints.text = "\(yourPoints)"
        case .computersVictory:
            lblResult.text = "You LOSE!"
            view.backgroundColor = .red
            computerPoints += 1
            lblComputerPoints.text = "\(computerPoints)"
        case .even:
            lblResult.text = "Play again..."
            view.backgroundColor = .lightText
        }
    }
    
}

