//
//  ViewController.swift
//  filmovi
//
//  Created by Aleksandra Lazarevic on 22.3.22..
//

import UIKit
import SafariServices

struct Film {
    var naziv: String
    var nazivSlike: String
    var godina: Int
    var imdbURL: String
}

class FilmCell: UITableViewCell {
    
    @IBOutlet weak var filmimgv: UIImageView!
    
    @IBOutlet weak var lblGodina: UILabel!
    @IBOutlet weak var lblNaziv: UILabel!
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SFSafariViewControllerDelegate {

    
    @IBOutlet weak var tableView: UITableView!
    
    var filmovi = [
        Film(naziv: "Gilmmer man", nazivSlike: "glimmerman", godina: 1996, imdbURL: "https://www.imdb.com/title/tt0116421/"),
        Film(naziv: "Star is born", nazivSlike: "star", godina: 2018, imdbURL: "https://www.imdb.com/title/tt1517451/"),
        Film(naziv: "Brazil", nazivSlike: "brazil", godina: 1985, imdbURL: "https://www.imdb.com/title/tt0088846/"),
        Film(naziv: "Parasite", nazivSlike: "parasite", godina: 2019, imdbURL: "https://www.imdb.com/title/tt6751668/"),
        Film(naziv: "Click", nazivSlike: "click", godina: 2006, imdbURL: "https://www.imdb.com/title/tt0389860/")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "filmCell") as! FilmCell
        
        let rowData = filmovi[indexPath.row]
        
        cell.lblNaziv.text = rowData.naziv
        cell.filmimgv.image = UIImage(named: rowData.nazivSlike)
        cell.lblGodina.text = "\(rowData.godina)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filmovi.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let safariVC = SFSafariViewController(url: URL(string: filmovi[indexPath.row].imdbURL)!)
        self.present(safariVC, animated: true, completion: nil)
        
    }

}

