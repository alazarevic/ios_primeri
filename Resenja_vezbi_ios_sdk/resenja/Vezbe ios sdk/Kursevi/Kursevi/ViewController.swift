//
//  ViewController.swift
//  Kursevi
//
//  Created by Aleksandra Lazarevic on 22.3.22..
//

import UIKit

class SmeroviCell: UITableViewCell {
    
    @IBOutlet weak var imgvSmer: UIImageView!
    
    @IBOutlet weak var lblNazivSmera: UILabel!
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return smerovi.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "smeroviCell") as! SmeroviCell
        
        let rowData = smerovi[indexPath.row]
        
        cell.lblNazivSmera.text = rowData.nazivSmera
        cell.imgvSmer.image = UIImage(data: rowData.slika)
        
        return cell
    }
    var selektovanSmer: Smer?
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selektovanSmer = smerovi[indexPath.row]
        performSegue(withIdentifier: "detaljiSmera", sender: self)
        tableView.deselectRow(at: indexPath, animated: true)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detaljiSmera" {
            let destinacija = segue.destination as! KurseviViewController
            destinacija.odabranSmer = selektovanSmer!
        }
    }
}

