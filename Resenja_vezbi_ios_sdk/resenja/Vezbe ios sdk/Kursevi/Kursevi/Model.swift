//
//  Model.swift
//  Kursevi
//
//  Created by Aleksandra Lazarevic on 22.3.22..
//

import Foundation

struct Kurs {
    var nazivKursa: String
    var opis: String
    var slika: Data
    var cena: Int
}

struct Smer {
    var nazivSmera: String
    var kursevi: [Kurs]
    var slika: Data
}

let iosSmer = Smer(nazivSmera: "iOS Development", kursevi: iosKursevi, slika: try! Data(contentsOf: URL(string: "https://upload.wikimedia.org/wikipedia/commons/thumb/c/ca/IOS_logo.svg/300px-IOS_logo.svg.png")!))
let javaSmer = Smer(nazivSmera: "Java Development", kursevi: javaKursevi, slika: try! Data(contentsOf: URL(string: "https://www.capital.ba/wp-content/uploads/2013/01/java.jpg")!))
let vebDizajnSmer = Smer(nazivSmera: "Web design", kursevi: vebDizajnKursevi, slika: try! Data(contentsOf: URL(string: "https://glasbans.com/assets/images/services/web-design-01.jpg")!))

let smerovi = [iosSmer, javaSmer, vebDizajnSmer]

var iosKursevi = [
    Kurs(nazivKursa: "Introduction to Swift", opis: "Kurs obradjuje osnove programskog jezika Swift", slika: try! Data(contentsOf: URL(string: "https://developer.apple.com/swift/images/swift-og.png")!), cena: 500),
    Kurs(nazivKursa: "Introduction to iOS SDK", opis: "Upoznavanje sa UIKit frameworkom i Xcode projektom i SDK.", slika: try! Data(contentsOf: URL(string: "https://cms-assets.tutsplus.com/uploads/users/41/posts/15769/preview_image/preview-image@2x.jpg")!), cena: 400),
    Kurs(nazivKursa: "iOS Project", opis: "Rad na projektu i kreiranje iOS aplikacije", slika: try! Data(contentsOf: URL(string: "https://miro.medium.com/max/1400/1*0VHpQVFiLBZ5cPac6OYAIQ.png")!), cena: 400)
]

var javaKursevi = [
    Kurs(nazivKursa: "Introduction to Java Programming", opis: "Upoznavanje sa programskim jezikom java.", slika: try! Data(contentsOf: URL(string: "https://www.capital.ba/wp-content/uploads/2013/01/java.jpg")!), cena: 450),
    Kurs(nazivKursa: "Advanced Java Programming", opis: "Napredne tehnike programiranja u java programskom jeziku.", slika: try! Data(contentsOf: URL(string: "https://www.mitrais.com/wp-content/uploads/2020/05/Java-Programmer.jpg")!), cena: 500),
    Kurs(nazivKursa: "Java web programming", opis: "Kreiranje web aplikacija upotrebom java programskog jezika.", slika: try! Data(contentsOf: URL(string: "https://www.ckcc.edu.kh/images/ICT-gellary/web-logo.jpg")!), cena: 500)
]

var vebDizajnKursevi = [
    Kurs(nazivKursa: "Theory of Design", opis: "Kurs upoznaje polaznike sa teorijom dizajna generalno ali i veb dizajna", slika: try! Data(contentsOf: URL(string: "https://storage.googleapis.com/gd-wagtail-prod-assets/original_images/Design-Guide-IO_3X2.png")!), cena: 200),
    Kurs(nazivKursa: "Introduction to HTML and CSS", opis: "Upoznavanje sa HTML i CSS i kreiranje osnovnih veb strana", slika: try! Data(contentsOf: URL(string: "https://miro.medium.com/max/1400/0*aVQR9kZAltUzmu8e")!), cena: 400),
    Kurs(nazivKursa: "JavaScript", opis: "Upoznavanje sa JavaScript jezikom i prakticna upotreba.", slika: try! Data(contentsOf: URL(string: "https://www.oxfordwebstudio.com/user/pages/06.da-li-znate/sta-je-javascript/sta-je-javascript.jpg")!), cena: 400)
]
