//
//  KurseviViewController.swift
//  Kursevi
//
//  Created by Aleksandra Lazarevic on 22.3.22..
//

import UIKit

class KursCell: UITableViewCell {
    
    @IBOutlet weak var imgvKurs: UIImageView!
    @IBOutlet weak var lblNazivKursa: UILabel!
    @IBOutlet weak var lblCena: UILabel!
}

class KurseviViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var odabranSmer: Smer?

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "kursCell") as! KursCell
        let rowData = odabranSmer!.kursevi[indexPath.row]
        cell.imgvKurs.image = UIImage(data: rowData.slika)
        cell.lblNazivKursa.text = rowData.nazivKursa
        cell.lblCena.text = "\(rowData.cena)$"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return odabranSmer!.kursevi.count
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
