//
//  ViewController.swift
//  potrosnja
//
//  Created by Aleksandra Lazarevic on 22.3.22..
//

import UIKit

struct Potrosnja {
    var automobil: String
    var datum: String
    var potrosnjaRezultat: Double
}

class PotrosnjaCell: UITableViewCell {
    
    @IBOutlet weak var lblAutomobil: UILabel!
    @IBOutlet weak var lblRezultat: UILabel!
    @IBOutlet weak var lblDatum: UILabel!
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    var potrosnja = [Potrosnja]()

    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var tfKilometri: UITextField!
    
    @IBOutlet weak var dpDatum: UIDatePicker!
    @IBOutlet weak var tfAutomobil: UITextField!
    @IBOutlet weak var tfLitri: UITextField!
    
    
    @IBOutlet weak var btnIzracunaj: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "potrosnjaCell") as! PotrosnjaCell
        
        let rowData = potrosnja[indexPath.row]
        
        cell.lblAutomobil.text = rowData.automobil
        cell.lblDatum.text = rowData.datum
        cell.lblRezultat.text = "\(rowData.potrosnjaRezultat) l/100 km "
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return potrosnja.count
    }
    

    
    @IBAction func btnIzracunajTapped(_ sender: Any) {
        let brojPotrosenog = Double(tfLitri.text!)!
        let kilometraza = Double(tfKilometri.text!)!
        let rezultat = brojPotrosenog / kilometraza * 100
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        
        let formatiranDatum = dateFormatter.string(from: dpDatum.date)
        
        let formatiranRezultat = rezultat.round(to: 2)
        
        let novaPotrosnja = Potrosnja(automobil: tfAutomobil.text!, datum: formatiranDatum, potrosnjaRezultat: formatiranRezultat)
        
        potrosnja.append(novaPotrosnja)
        tableView.reloadData()
    }
    
}

extension Double {
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
